import math
import pickle
import random
import time
from abc import abstractmethod
from datetime import datetime

import metrics
import neural
import numpy as np
import scipy
from gurobipy import *
from sklearn.cluster import KMeans
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import StandardScaler
from uta import PairwiseUTA


class BaseModel(object):
    """
    Base class for models, to be used as coding pattern skeleton.
    Can be used for a model on a single cluster or on multiple clusters"""

    def __init__(self):
        """Initialization of your model and its hyper-parameters"""
        pass

    @abstractmethod
    def fit(self, X, Y):
        """Fit function to find the parameters according to (X, Y) data.
        (X, Y) formatting must be so that X[i] is preferred to Y[i] for all i.

        Parameters
        ----------
        X: np.ndarray
            (n_samples, n_features) features of elements preferred to Y elements
        Y: np.ndarray
            (n_samples, n_features) features of unchosen elements
        """
        # Customize what happens in the fit function
        return

    @abstractmethod
    def predict_utility(self, X):
        """Method to call the decision function of your model

        Parameters:
        -----------
        X: np.ndarray
            (n_samples, n_features) list of features of elements
        """
        # Customize what happens in the predict utility function
        return

    def predict_preference(self, X, Y):
        """Method to predict which pair is preferred between X[i] and Y[i] for all i.
        Returns a preference for each cluster.

        Parameters
        -----------
        X: np.ndarray
            (n_samples, n_features) list of features of elements to compare with Y elements of same index
        Y: np.ndarray
            (n_samples, n_features) list of features of elements to compare with X elements of same index

        Returns
        -------
        np.ndarray:
            (n_samples, n_clusters) array of preferences for each cluster. 1 if X is preferred to Y, 0 otherwise
        """
        X_u = self.predict_utility(X)
        Y_u = self.predict_utility(Y)

        return (X_u - Y_u > 0).astype(int)

    def predict_cluster(self, X, Y):
        """Predict which cluster prefers X over Y THE MOST, meaning that if several cluster prefer X over Y, it will
        be assigned to the cluster showing the highest utility difference). The reversal is True if none of the clusters
        prefer X over Y.
        Compared to predict_preference, it indicates a cluster index.

        Parameters
        -----------
        X: np.ndarray
            (n_samples, n_features) list of features of elements to compare with Y elements of same index
        Y: np.ndarray
            (n_samples, n_features) list of features of elements to compare with X elements of same index

        Returns
        -------
        np.ndarray:
            (n_samples, ) index of cluster with highest preference difference between X and Y.
        """
        X_u = self.predict_utility(X)
        Y_u = self.predict_utility(Y)

        return np.argmax(X_u - Y_u, axis=1)

    def save_model(self, path):
        """Save the model in a pickle file. Don't hesitate to change it in the child class if needed

        Parameters
        ----------
        path: str
            path indicating the file in which the model will be saved
        """
        with open(path, "wb") as f:
            pickle.dump(self, f)

    @classmethod
    def load_model(clf, path):
        """Load a model saved in a pickle file. Don't hesitate to change it in the child class if needed

        Parameters
        ----------
        path: str
            path indicating the path to the file to load
        """
        with open(path, "rb") as f:
            model = pickle.load(f)
        return model


class RandomExampleModel(BaseModel):
    """Example of a model on two clusters, drawing random coefficients.
    You can use it to understand how to write your own model and the data format that we are waiting for.
    This model does not work well but you should have the same data formatting with TwoClustersMIP.
    """

    def __init__(self):
        self.seed = 444
        self.weights = self.instantiate()

    def instantiate(self):
        """No particular instantiation"""
        return []

    def fit(self, X, Y):
        """fit function, sets random weights for each cluster. Totally independant from X & Y.

        Parameters
        ----------
        X: np.ndarray
            (n_samples, n_features) features of elements preferred to Y elements
        Y: np.ndarray
            (n_samples, n_features) features of unchosen elements
        """
        np.random.seed(self.seed)
        indexes = np.random.randint(0, 2, (len(X)))
        num_features = X.shape[1]
        weights_1 = np.random.rand(num_features)
        weights_2 = np.random.rand(num_features)

        weights_1 = weights_1 / np.sum(weights_1)
        weights_2 = weights_2 / np.sum(weights_2)
        self.weights = [weights_1, weights_2]
        return self

    def predict_utility(self, X):
        """Simple utility function from random weights.

        Parameters:
        -----------
        X: np.ndarray
            (n_samples, n_features) list of features of elements
        """
        return np.stack(
            [np.dot(X, self.weights[0]), np.dot(X, self.weights[1])], axis=1
        )


class TwoClustersMIP(BaseModel):
    """Skeleton of MIP you have to write as the first exercise.
    You have to encapsulate your code within this class that will be called for evaluation.
    """

    def __init__(self, n_pieces, n_clusters, precision=0.0001):
        """Initialization of the MIP Variables

        Parameters
        ----------
        n_pieces: int
            Number of pieces for the utility function of each feature.
        n°clusters: int
            Number of clusters to implement in the MIP.
        """
        self.seed = 123
        self.K = n_clusters
        self.L = n_pieces
        self.epsilon = precision
        self.model = self.instantiate()
        # self.model.setParam(GRB.Param.FeasibilityTol, precision * 2)

    def instantiate(self):
        """Instantiation of the MIP Variables - To be completed."""
        # To be completed
        model = Model("Simple PL modelling")
        return model

    def u_k_i(self, k, i, X, values: bool = False):
        x = X[i]
        if x == self.maxs[i]:
            return self.Us[k][i][-1] if not values else self.Us[k][i][-1].X
        l = math.floor(self.L * (x - self.mins[i]) / (self.maxs[i] - self.mins[i]))
        x_l = self.mins[i] + l * (self.maxs[i] - self.mins[i]) / self.L
        width = x - x_l

        ukil = self.Us[k][i][l] if not values else self.Us[k][i][l].X
        ukilp1 = self.Us[k][i][l + 1] if not values else self.Us[k][i][l + 1].X

        delta = ukilp1 - ukil

        slope = delta * self.L / (self.maxs[i] - self.mins[i])
        val = ukil + slope * width
        return val

    def u_k(self, k, X, values: bool = False):
        if not values:
            return quicksum(self.u_k_i(k, i, X, values=False) for i in range(self.n))
        else:
            return sum(self.u_k_i(k, i, X, values=True) for i in range(self.n))

    def fit(self, X, Y):
        """Estimation of the parameters - To be completed.

        Parameters
        ----------
        X: np.ndarray
            (n_samples, n_features) features of elements preferred to Y elements
        Y: np.ndarray
            (n_samples, n_features) features of unchosen elements
        """
        n_samples, n_criteria = X.shape

        self.n = n_criteria

        self.Xs = [self.model.addVar(name=f"x_{i}") for i in range(self.n)]
        self.Us = [
            [
                [self.model.addVar(name=f"u_{k}_{i}_{l}") for l in range(self.L + 1)]
                for i in range(self.n)
            ]
            for k in range(self.K)
        ]
        # Function must be non-decreasing
        for k, i, l in zip(range(self.K), range(self.n), range(self.L - 1)):
            self.model.addConstr(self.Us[k][i][l] <= self.Us[k][i][l + 1])
        # self.model.setObjective(sum(sigma_plus) + sum(sigma_minus), GRB.MAXIMIZE)

        self.mins = np.minimum(X.min(axis=0), Y.min(axis=0))
        self.maxs = np.maximum(X.max(axis=0), Y.max(axis=0))

        self.z = []

        # Set min and max values
        for k in range(self.K):
            for i in range(self.n):
                self.model.addConstr(self.Us[k][i][0] == 0)
            self.model.addConstr(
                quicksum(self.Us[k][i][-1] for i in range(self.n)) == 1
            )

        self.or_constraint_variables = []

        self.underestimation_variables = []
        self.overestimation_variables = []
        M = 3

        # To be completed
        for index, couple in enumerate(zip(X, Y)):
            self.z.append(self.model.addVar(name=f"one_of_clusters_constraint_{index}"))
            x, y = couple

            var_acc = []
            for k in range(self.K):
                b = self.model.addVar(
                    name=f"binary_indicator_{index}_cluster_{k}", vtype=GRB.BINARY
                )
                splusx = self.model.addVar(name=f"overestimation_x_{index}_cluster_{k}")
                sminusx = self.model.addVar(
                    name=f"underestimation_x_{index}_cluster_{k}"
                )

                splusy = self.model.addVar(name=f"overestimation_y_{index}_cluster_{k}")
                sminusy = self.model.addVar(
                    name=f"underestimation_y_{index}_cluster_{k}"
                )

                self.overestimation_variables.extend((splusx, splusy))
                self.underestimation_variables.extend((sminusx, sminusy))
                self.model.addConstr(splusx >= 0)
                self.model.addConstr(splusy >= 0)
                self.model.addConstr(sminusx >= 0)
                self.model.addConstr(sminusy >= 0)
                # expr: self.u_k(k, x) - splusx + sminusx
                # expr2: (self.u_k(k, y) - splusy + sminusy + self.epsilon)

                # (expr > expr2) ==> b = 1
                # équivalent à : b = 0 ==> (expr <= expr2)

                # et (expr <= expr2) ==> b = 0
                # équivalent à : b = 1 ==> (expr > expr2)
                # on peut donc utiliser une indicator constraint

                # Majorant

                self.model.addConstr(
                    0
                    <= (self.u_k(k, x) - splusx + sminusx)
                    - (self.u_k(k, y) - splusy + sminusy)
                    + M * (1 - b)
                )
                var_acc.append(b)
            self.model.addConstr(self.z[index] == quicksum(var_acc))
            self.model.addConstr(self.z[index] >= 1)
            self.or_constraint_variables.extend(var_acc)

        for k in range(self.K):
            for l in range(self.L - 1):
                for i in range(self.n):
                    self.model.addConstr(
                        self.Us[k][i][l + 1] - self.Us[k][i][l] >= self.epsilon
                    )

        self.model.setObjective(
            quicksum(self.overestimation_variables + self.underestimation_variables),
            GRB.MINIMIZE,
        )
        self.model.optimize()
        return

    def predict_utility(self, X):
        """Return Decision Function of the MIP for X. - To be completed.

        Parameters:
        -----------
        X: np.ndarray
            (n_samples, n_features) list of features of elements
        """
        # To be completed
        # Do not forget that this method is called in predict_preference (line 42) and therefor should return well-organized data for it to work.
        if len(X.shape) == 1:
            criteria = [self.u_k(k, X, values=True) for k in range(self.K)]
        else:
            criteria = np.apply_along_axis(
                lambda x: [self.u_k(k, x, values=True) for k in range(self.K)],
                axis=1,
                arr=X,
            )

        return criteria


class EMModel(BaseModel):
    """Models based on (variants) of the Expectation Maximisation algorithm"""

    def compute_weights(self, data_to_cluster):
        self.cluster_gaussians = [
            scipy.stats.multivariate_normal(
                self.cluster_means[k], self.covariance_matrices[k]
            )
            for k in range(self.K)
        ]
        self.cluster_pdfs = [
            self.cluster_gaussians[k].pdf(data_to_cluster) for k in range(self.K)
        ]
        self.p_total = sum(self.cluster_pdfs)
        self.weights = [self.cluster_pdfs[k] / self.p_total for k in range(self.K)]

    def compute_new_distribution(self, data_to_cluster):

        self.cluster_means = [
            self.weights[k] @ data_to_cluster / np.sum(self.weights[k])
            for k in range(self.K)
        ]

        self.cluster_stddev = [
            np.sqrt(
                self.weights[k]
                @ (data_to_cluster - self.cluster_means[k]) ** 2
                / np.sum(self.weights[k])
            )
            for k in range(self.K)
        ]

        self.covariance_matrices = [
            np.diag(self.cluster_stddev[k] ** 2) for k in range(self.K)
        ]


class ExpectationMaximisation:
    def __init__(self, n_clusters: int, convergence_threshold=0.99, max_iterations=500):
        self.K = n_clusters
        self.convergence_threshold = convergence_threshold
        self.max_iterations = max_iterations

    def compute_weights(self, data_to_cluster):
        self.cluster_gaussians = [
            scipy.stats.multivariate_normal(
                self.cluster_means[k], self.covariance_matrices[k]
            )
            for k in range(self.K)
        ]
        self.cluster_pdfs = [
            self.cluster_gaussians[k].pdf(data_to_cluster) for k in range(self.K)
        ]
        self.p_total = sum(self.cluster_pdfs)
        self.weights = [self.cluster_pdfs[k] / self.p_total for k in range(self.K)]

    def compute_new_distribution(self, data_to_cluster):

        self.cluster_means = [
            self.weights[k] @ data_to_cluster / np.sum(self.weights[k])
            for k in range(self.K)
        ]

        self.cluster_stddev = [
            np.sqrt(
                self.weights[k]
                @ (data_to_cluster - self.cluster_means[k]) ** 2
                / np.sum(self.weights[k])
            )
            for k in range(self.K)
        ]

        self.covariance_matrices = [
            np.diag(self.cluster_stddev[k] ** 2) for k in range(self.K)
        ]

    def fit(self, data_to_cluster):
        cluster_convs = []
        n_samples, n_criteria = data_to_cluster.shape

        self.mins = data_to_cluster.min(axis=0)
        self.maxs = data_to_cluster.max(axis=0)

        self.cluster_means = [
            np.array(
                [random.uniform(self.mins[i], self.maxs[i]) for i in range(n_criteria)]
            )
            for k in range(self.K)
        ]
        self.cluster_stddev = [
            np.array([1 for i in range(n_criteria)]) for k in range(self.K)
        ]

        self.covariance_matrices = [
            np.diag(self.cluster_stddev[k] ** 2) for k in range(self.K)
        ]

        self.labels = np.zeros(shape=(n_samples,))

        self.compute_weights(data_to_cluster)

        cluster_convergence = 0
        iterations = 0

        while (
            cluster_convergence < self.convergence_threshold
            or iterations < self.max_iterations
        ):

            self.compute_new_distribution(data_to_cluster)

            self.compute_weights(data_to_cluster)

            previous_labels = self.labels
            self.labels = np.argmax(np.array(self.cluster_pdfs).T, axis=1)
            cluster_convergence = np.sum(previous_labels == self.labels) / n_samples
            cluster_convs.append(cluster_convergence)
            iterations += 1

        return self.labels


class HeuristicKMeansModel(BaseModel):
    """Heuristic model with KMeans clustering"""

    def __init__(self, n_clusters, n_pieces=5, **kwargs):
        """Initialization of the Heuristic Model."""
        self.seed = 123
        self.K = n_clusters
        self.L = n_pieces
        self.instantiate()

    def instantiate(self):
        """Instantiation of the MIP Variables"""
        # To be completed
        self.preprocessing = Pipeline(("std_scaler", StandardScaler()))
        self.k_means = KMeans(n_clusters=self.K)
        self.utility_functions = [k for k in range(self.K)]
        return

    def fit(self, X, Y, labels=None, verbose: bool = False):
        """Estimation of the parameters - To be completed.

        Parameters
        ----------
        X: np.ndarray
            (n_samples, n_features) features of elements preferred to Y elements
        Y: np.ndarray
            (n_samples, n_features) features of unchosen elements
        """
        # To be completed
        start = time.time()

        self.xymins = np.minimum(X.min(axis=0), Y.min(axis=0))
        self.xymaxs = np.maximum(X.max(axis=0), Y.max(axis=0))

        prefs = np.hstack((X, Y))

        n_samples, n_criteria = X.shape
        self.n = n_criteria

        diff = X - Y

        # We cluster X/Y differences and f(x) - f(Y)
        data_to_cluster = diff

        self.k_means = KMeans(n_clusters=self.K)

        self.k_means.fit(data_to_cluster)

        self.labels = self.k_means.labels_

        self.UTAs = [
            PairwiseUTA(
                n_criteria=self.n,
                n_pieces=self.L,
                force_mins=self.xymins,
                force_maxs=self.xymaxs,
            )
            for k in range(self.K)
        ]

        for cluster in range(self.K):
            print(f"------------------ TRAINING CLUSTER {cluster} ------------------ ")
            cluster_X = X[self.labels == cluster, :]
            cluster_Y = Y[self.labels == cluster, :]

            self.UTAs[cluster].fit(cluster_X, cluster_Y)

        return

    def predict_utility(self, X):
        """Return Decision Function of the MIP for X. - To be completed.

        Parameters:
        -----------
        X: np.ndarray
            (n_samples, n_features) list of features of elements
        """
        # To be completed
        # Do not forget that this method is called in predict_preference (line 42) and therefor should return well-organized data for it to work.

        if len(X.shape) == 1:
            return [self.UTAs[k].predict_utility(X) for k in range(self.K)]

        else:

            criteria = np.apply_along_axis(
                lambda x: [self.UTAs[k].predict_utility(x) for k in range(self.K)],
                axis=1,
                arr=X,
            )

        return criteria


class HeuristicEMModel(BaseModel):
    """Skeleton of MIP you have to write as the first exercise.
    You have to encapsulate your code within this class that will be called for evaluation.
    """

    def __init__(self, n_clusters, n_pieces=5, **kwargs):
        """Initialization of the Heuristic Model."""
        self.seed = 123
        self.K = n_clusters
        self.L = n_pieces
        self.instantiate()

    def instantiate(self):
        """Instantiation of the MIP Variables"""
        # To be completed
        self.preprocessing = Pipeline(("std_scaler", StandardScaler()))
        self.k_means = KMeans(n_clusters=self.K)
        self.utility_functions = [k for k in range(self.K)]
        return

    def fit(self, X, Y, labels=None, verbose: bool = False):
        """Estimation of the parameters - To be completed.

        Parameters
        ----------
        X: np.ndarray
            (n_samples, n_features) features of elements preferred to Y elements
        Y: np.ndarray
            (n_samples, n_features) features of unchosen elements
        """
        # To be completed
        start = time.time()

        self.xymins = np.minimum(X.min(axis=0), Y.min(axis=0))
        self.xymaxs = np.maximum(X.max(axis=0), Y.max(axis=0))

        prefs = np.hstack((X, Y))

        n_samples, n_criteria = X.shape
        self.n = n_criteria

        diff = X - Y

        # We cluster X/Y differences and f(x) - f(Y)
        data_to_cluster = diff

        self.em = ExpectationMaximisation(
            n_clusters=3, convergence_threshold=0.99, max_iterations=500
        )

        self.labels = self.em.fit(data_to_cluster)

        self.UTAs = [
            PairwiseUTA(
                n_criteria=self.n,
                n_pieces=self.L,
                force_mins=self.xymins,
                force_maxs=self.xymaxs,
            )
            for k in range(self.K)
        ]

        for cluster in range(self.K):
            print(f"------------------ TRAINING CLUSTER {cluster} ------------------ ")
            cluster_X = X[self.labels == cluster, :]
            cluster_Y = Y[self.labels == cluster, :]

            self.UTAs[cluster].fit(cluster_X, cluster_Y)

        return

    def predict_utility(self, X):
        """Return Decision Function of the MIP for X. - To be completed.

        Parameters:
        -----------
        X: np.ndarray
            (n_samples, n_features) list of features of elements
        """
        # To be completed
        # Do not forget that this method is called in predict_preference (line 42) and therefor should return well-organized data for it to work.

        if len(X.shape) == 1:
            return [self.UTAs[k].predict_utility(X) for k in range(self.K)]

        else:

            criteria = np.apply_along_axis(
                lambda x: [self.UTAs[k].predict_utility(x) for k in range(self.K)],
                axis=1,
                arr=X,
            )

        return criteria


class HeuristicEMAModel(EMModel):
    """Mixed model with UTA function learning in the middle of the EM algorithm"""

    def __init__(self, n_clusters, **kwargs):
        """Initialization of the Heuristic Model."""
        self.seed = 123
        self.K = n_clusters
        self.instantiate()

    def instantiate(self):
        """Instantiation of the MIP Variables"""
        # To be completed
        self.k_means = KMeans(n_clusters=self.K)
        return

    def fit(self, X, Y, labels=None, iterations=30, verbose: bool = False):
        """Estimation of the parameters - To be completed.

        Parameters
        ----------
        X: np.ndarray
            (n_samples, n_features) features of elements preferred to Y elements
        Y: np.ndarray
            (n_samples, n_features) features of unchosen elements
        """
        # To be completed

        self.xymins = np.minimum(X.min(axis=0), Y.min(axis=0))
        self.xymaxs = np.maximum(X.max(axis=0), Y.max(axis=0))

        pairs_explained = metrics.PairsExplained()
        cluster_intersection = metrics.ClusterIntersection()

        prefs = np.hstack((X, Y))

        results = {"explained": [], "grouped": [], "cluster_convergence": []}

        n_samples, n_criteria = X.shape
        self.n = n_criteria

        diff = X - Y

        # We cluster X/Y differences and f(x) - f(Y)
        data_to_cluster = np.hstack(
            (diff, np.random.normal(loc=0, scale=1, size=(n_samples, 3)))
        )

        self.mins = diff.min(axis=0)
        self.maxs = diff.max(axis=0)

        # Random initialisations within boundaries for clusters
        self.cluster_means = [
            np.array(
                [random.uniform(self.mins[i], self.maxs[i]) for i in range(self.n)]
                + [0 for j in range(self.K)]
            )
            for k in range(self.K)
        ]
        self.cluster_stddev = [
            np.array([1 for i in range(n_criteria)] + [1 for j in range(self.K)])
            for k in range(self.K)
        ]

        self.covariance_matrices = [
            np.diag(self.cluster_stddev[k] ** 2) for k in range(self.K)
        ]

        self.labels = np.zeros(shape=(n_samples,))

        self.compute_weights(data_to_cluster)

        print("INITIAL CLUSTER STATES:")
        for k in range(self.K):
            print(f"µ{k} = {self.cluster_means[k]}")
            print(f"σ{k} = {self.cluster_stddev[k]}")

        print(
            f"############################### CLUSTER INITIALISATION ###############################"
        )

        for i in range(iterations):

            print(
                f"################################ ITERATION {i} #################################"
            )
            self.compute_new_distribution(data_to_cluster)

            self.compute_weights(data_to_cluster)

            previous_labels = self.labels
            self.labels = np.argmax(np.array(self.cluster_pdfs).T, axis=1)
            cluster_convergence = np.sum(previous_labels == self.labels) / n_samples
            print(f"cluster_convergence : {cluster_convergence}")
            results["cluster_convergence"].append(cluster_convergence)

            print("CLUSTER STATES:")
            for k in range(self.K):
                print(f"µ{k} = {self.cluster_means[k]}")
                print(f"σ{k} = {self.cluster_stddev[k]}")

            self.UTAs = [
                PairwiseUTA(
                    n_criteria=self.n,
                    n_pieces=self.L,
                    force_mins=self.xymins,
                    force_maxs=self.xymaxs,
                )
                for k in range(self.K)
            ]

            for cluster in range(self.K):
                print(
                    f"------------------ TRAINING CLUSTER {cluster} ------------------ "
                )
                cluster_X = X[self.labels == cluster, :]
                cluster_Y = Y[self.labels == cluster, :]
                # split_pairs = [[pref[: self.n], pref[self.n :]] for pref in cluster_pairs]

                self.UTAs[cluster].fit(cluster_X, cluster_Y)

            eval_X = self.predict_utility(X)
            eval_Y = self.predict_utility(Y)

            if labels is not None:
                explained = pairs_explained.from_model(self, X, Y)
                grouped = cluster_intersection.from_model(self, X, Y, labels)

                print(
                    f"Iter {i} - Percentage of explained preferences on train data:",
                    explained,
                )  # You should get 1.0 with the right MIP
                print(
                    f"Iter {i} - Percentage of preferences well regrouped into clusters:",
                    grouped,
                )
                results["explained"].append(explained)
                results["grouped"].append(grouped)

            data_to_cluster = np.hstack((diff, eval_X - eval_Y))
            print(data_to_cluster[:, 10:])

        print(results)

        return

    def fit_eval(self, X, Y, Z):
        self.fit(X, Y, labels=Z)

    def predict_utility(self, X):
        """Return Decision Function of the MIP for X. - To be completed.

        Parameters:
        -----------
        X: np.ndarray
            (n_samples, n_features) list of features of elements
        """
        # To be completed
        # Do not forget that this method is called in predict_preference (line 42) and therefor should return well-organized data for it to work.

        if len(X.shape) == 1:
            return [self.UTAs[k].predict_utility(X) for k in range(self.K)]

        else:

            criteria = np.apply_along_axis(
                lambda x: [self.UTAs[k].predict_utility(x) for k in range(self.K)],
                axis=1,
                arr=X,
            )

        return criteria


class HeuristicNeuralModelEM(EMModel):
    """Skeleton of MIP you have to write as the first exercise.
    You have to encapsulate your code within this class that will be called for evaluation.
    """

    def __init__(self, n_clusters, **kwargs):
        """Initialization of the Heuristic Model."""
        self.seed = 123
        self.K = n_clusters
        self.instantiate()

    def instantiate(self):
        """Instantiation of the MIP Variables"""
        # To be completed
        return

    def eval_all_models(self, arr):
        results = [
            neural.batch_apply_neural(model, arr, batch_size=1000)
            for model in self.neural_models
        ]
        return np.concatenate(results, axis=1)

    def fit(self, X, Y, labels=None, verbose: bool = False):
        """Estimation of the parameters - To be completed.

        Parameters
        ----------
        X: np.ndarray
            (n_samples, n_features) features of elements preferred to Y elements
        Y: np.ndarray
            (n_samples, n_features) features of unchosen elements
        """

        n_samples, n_criteria = X.shape
        self.n = n_criteria

        diff = X - Y

        # We cluster X/Y differences
        data_to_cluster = diff

        self.em = ExpectationMaximisation(
            n_clusters=self.K, convergence_threshold=0.99, max_iterations=500
        )

        self.labels = self.em.fit(data_to_cluster)

        lr = 0.05
        tau = 5
        batch_size = 50
        epochs = 2

        self.neural_models = [None for k in range(self.K)]

        for cluster in range(self.K):

            print(f"------------------ TRAINING CLUSTER {cluster} ------------------ ")
            cluster_X = X[self.labels == cluster, :]
            cluster_Y = Y[self.labels == cluster, :]

            # X_train, X_test, Y_train, Y_test = neural.split_data(cluster_X, cluster_Y)

            model = neural.train(
                cluster_X.astype(np.float32),
                cluster_Y.astype(np.float32),
                None,
                None,
                learning_rate=lr,
                loss_tau=tau,
                batch_size=batch_size,
                epochs=epochs,
                verbose=verbose,
            )

            self.neural_models[cluster] = model

    def fit_eval(self, X, Y, Z):
        self.fit(X, Y, labels=Z)

    def predict_utility(self, X):
        """Return Decision Function of the MIP for X. - To be completed.

        Parameters:
        -----------
        X: np.ndarray
            (n_samples, n_features) list of features of elements
        """
        # To be completed
        # Do not forget that this method is called in predict_preference (line 42) and therefor should return well-organized data for it to work.

        if len(X.shape) == 1:
            return self.eval_all_models(X).reshape((self.n,))
        else:
            return self.eval_all_models(X)


class HeuristicNeuralModelKMeans(BaseModel):
    """Skeleton of MIP you have to write as the first exercise.
    You have to encapsulate your code within this class that will be called for evaluation.
    """

    def __init__(self, n_clusters):
        """Initialization of the Heuristic Model."""
        self.seed = 123
        self.K = n_clusters
        self.instantiate()

    def instantiate(self):
        """Instantiation of the MIP Variables"""
        # To be completed
        return

    def eval_all_models(self, arr):
        results = [
            neural.batch_apply_neural(model, arr, batch_size=1000)
            for model in self.neural_models
        ]
        return np.concatenate(results, axis=1)

    def fit(self, X, Y, iterations=10, verbose: bool = False):
        """Estimation of the parameters - To be completed.

        Parameters
        ----------
        X: np.ndarray
            (n_samples, n_features) features of elements preferred to Y elements
        Y: np.ndarray
            (n_samples, n_features) features of unchosen elements
        """

        # To be completed
        # prefs = np.hstack((X, Y))
        results = {"explained": [], "grouped": []}
        diff = X - Y

        data_to_cluster = diff

        pairs_explained = metrics.PairsExplained()
        cluster_intersection = metrics.ClusterIntersection()

        self.k_means = KMeans(n_clusters=self.K)

        self.k_means.fit(data_to_cluster)

        labels = self.k_means.labels_

        self.neural_models = [None for k in range(self.K)]

        lr = 0.05
        tau = 5
        batch_size = 50
        epochs = 2

        for cluster in range(self.K):
            print(f"------------------ TRAINING CLUSTER {cluster} ------------------ ")
            cluster_X = X[labels == cluster, :]
            cluster_Y = Y[labels == cluster, :]

            # X_train, X_test, Y_train, Y_test = neural.split_data(cluster_X, cluster_Y)

            model = neural.train(
                cluster_X.astype(np.float32),
                cluster_Y.astype(np.float32),
                None,
                None,
                learning_rate=lr,
                loss_tau=tau,
                batch_size=batch_size,
                epochs=epochs,
                verbose=verbose,
            )

            self.neural_models[cluster] = model

        return

    def predict_utility(self, X):
        """Return Decision Function of the MIP for X. - To be completed.

        Parameters:
        -----------
        X: np.ndarray
            (n_samples, n_features) list of features of elements
        """
        # To be completed
        # Do not forget that this method is called in predict_preference (line 42) and therefor should return well-organized data for it to work.

        if len(X.shape) == 1:
            return self.eval_all_models(X).reshape((self.n,))
        else:
            return self.eval_all_models(X)


HeuristicModel = HeuristicNeuralModelKMeans

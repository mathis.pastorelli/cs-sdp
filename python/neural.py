from datetime import datetime

import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
from sklearn.model_selection import train_test_split
from torch.utils.data import DataLoader

from data import Dataloader

device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

DATA_SIZE = 10
NUM_HIDDEN_NEURONS_1 = 300
NUM_HIDDEN_NEURONS_2 = 50


class MLP(nn.Module):
    def __init__(self):
        super(MLP, self).__init__()
        self.fc1 = nn.Linear(DATA_SIZE, NUM_HIDDEN_NEURONS_1)
        self.fc2 = nn.Linear(NUM_HIDDEN_NEURONS_1, NUM_HIDDEN_NEURONS_2)
        self.out_layer = nn.Linear(NUM_HIDDEN_NEURONS_2, 1)

    def forward(self, x):
        x = x.view(-1, DATA_SIZE)  # passer de 28*28 à  (784,)
        x = F.relu(self.fc1(x))
        x = F.relu(self.fc2(x))
        x = self.out_layer(x)
        return x


class PairLoss(nn.modules.loss._Loss):
    def __init__(self, tau=5) -> None:
        super().__init__()
        self.tau = tau

    def forward(self, f_x: torch.Tensor, f_y: torch.Tensor) -> torch.Tensor:
        return torch.sum(F.sigmoid((f_y - f_x) / self.tau))


def train_model(
    X_train, Y_train, epoch, model, optimizer, loss_fn, batch_size=50, verbose=False
):
    x_train_dl = DataLoader(torch.from_numpy(X_train), batch_size=batch_size)
    y_train_dl = DataLoader(torch.from_numpy(Y_train), batch_size=batch_size)

    train_loader = zip(x_train_dl, y_train_dl)

    model.train()
    tot_loss = 0
    correct = 0
    for batch_idx, (x, y) in enumerate(train_loader):
        optimizer.zero_grad()
        x, y = x.to(device), y.to(device)
        F_x = model(x)
        F_y = model(y)
        loss = loss_fn(F_x, F_y)
        loss.backward()
        tot_loss += loss.item()
        optimizer.step()
        cur_correct = (F_x.detach().numpy() > F_y.detach().numpy()).sum()
        correct += cur_correct

        # if epoch == 0:
        # print(f"batch {batch_idx}: {100*cur_correct/batch_size} %")

    accuracy = 100.0 * correct / len(x_train_dl.dataset)

    if verbose:
        print(
            "epoch {} training loss: {} - tot accuracy {}".format(
                epoch, tot_loss, accuracy
            )
        )
    return tot_loss, accuracy


def test_model(
    X_test,
    Y_test,
    epoch,
    model,
    optimizer,
    loss_fn,
    batch_size=50,
    verbose: bool = False,
):
    x_test_dl = DataLoader(torch.from_numpy(X_test), batch_size=batch_size)
    y_test_dl = DataLoader(torch.from_numpy(Y_test), batch_size=batch_size)

    test_loader = zip(x_test_dl, y_test_dl)

    model.eval()
    correct = 0
    with torch.no_grad():
        tot_loss = 0

        for batch_idx, (x, y) in enumerate(test_loader):
            x, y = x.to(device), y.to(device)
            F_x = model(x)
            F_y = model(y)
            loss = loss_fn(F_x, F_y)

            tot_loss += loss.item()

            correct += (F_x.numpy() > F_y.numpy()).sum()

        accuracy = 100 * correct / len(x_test_dl.dataset)

        if verbose:
            print(
                "epoch {} test loss: {} — accuracy: {} %".format(
                    epoch, tot_loss, accuracy
                )
            )

        return tot_loss, accuracy


def split_data(X, Y, test_size=0.2):
    X_train, X_test, Y_train, Y_test = train_test_split(
        X.astype(np.float32), Y.astype(np.float32), test_size=test_size
    )
    return X_train, X_test, Y_train, Y_test


def train(
    X_train,
    Y_train,
    X_test=None,
    Y_test=None,
    learning_rate=0.05,
    loss_tau=5,
    batch_size=50,
    epochs=30,
    save_model=False,
    verbose: bool = False,
):
    """Train a multi-layer perceptron to fit the utility function that will maximize f(x) > f(y)."""
    model = MLP()
    model.to(device)

    optimizer = torch.optim.SGD(model.parameters(), lr=learning_rate)
    loss_fn = PairLoss(tau=loss_tau)

    model_path = f"heuristic_experiment_.pt"

    for epoch in range(epochs):
        # training
        train_loss, train_accuracy = train_model(
            X_train,
            Y_train,
            model=model,
            optimizer=optimizer,
            loss_fn=loss_fn,
            batch_size=batch_size,
            epoch=epoch,
            verbose=verbose,
        )
        # testing

        if X_test is not None:
            test_loss, test_accuracy = test_model(
                X_test,
                Y_test,
                model=model,
                optimizer=optimizer,
                loss_fn=loss_fn,
                batch_size=batch_size,
                epoch=epoch,
                verbose=verbose,
            )

        if save_model:
            torch.save(model.state_dict(), model_path)

        return model


def bulk(model, array, batch_size, start=None, end=None):
    model.eval()
    array = array.astype(np.float32)
    if start and end:
        inp = torch.from_numpy(array[start:end, :])
    elif start:
        inp = torch.from_numpy(array[start:, :])
    elif end:
        inp = torch.from_numpy(array[:end, :])
    else:
        inp = torch.from_numpy(array)
    with torch.no_grad():
        result = model(inp)
    return result.numpy()


def batch_apply_neural(mod, array, batch_size: int = 1000):
    mod.eval()
    array = array.astype(np.float32)
    results = []

    n_samples, n_criteria = array.shape
    with torch.no_grad():
        for start in range(0, n_samples, batch_size):
            batch = torch.from_numpy(
                array[start : start + min(batch_size, n_samples), :]
            )
            result = mod(batch)
            results.append(result.numpy())
    return np.concatenate(results, axis=0)
